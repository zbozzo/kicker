import {Component} from '@angular/core';
// import {MatomoInjector} from 'ngx-matomo';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kicker';
  public isMenuCollapsed  = true;
  tbcUrl = (environment.envVar.CI_SERVER_URL || 'https://gitlab.com') + '/' + (environment.envVar.CI_PROJECT_NAMESPACE || 'to-be-continuous');

  // constructor(
  //   private matomoInjector: MatomoInjector
  // ) {
  //   if (environment.matomo.enable) {
  //     this.matomoInjector.init(environment.matomo.url, environment.matomo.id);
  //   }
  // }
}
