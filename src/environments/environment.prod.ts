export const environment = {
  production: true,
  kickermodel: {
    // in production mode, kicker model is loaded from documentation pages (regularly regenerated)
    url: '/doc/kicker-aggregated.json'
  },
  matomo: {
    enable: false,
    url: 'https://matomo.host/',
    id: 666
  },
  envVar: {
    CI_SERVER_HOST: "gitlab.com",
    CI_SERVER_URL: "https://gitlab.com",
    CI_PROJECT_NAMESPACE: "to-be-continuous"
  }
};
